<?php

class Template {

	protected $CI;
	var $template_data = array();

	function __construct(){
		$this->CI =& get_instance();
	}

	function set($content_area, $value){
		$this->template_data[$content_area] = $value;
	}

	function load($template = '', $name = '', $view = '', $view_data = array(), $return = FALSE){
		$this->set($name, $this->CI->load->view($view, $view_data, TRUE));
		$this->CI->load->view('admin/partial/'.$template, $this->template_data);
	}

	function page403() {
		return $this->CI->load->view('err_403');
	}

}

?>