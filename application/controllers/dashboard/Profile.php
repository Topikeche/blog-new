<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->utils->isLogin()) redirect('auth/signin');

		$this->load->model('User_model');
	}

	public function index() {
		$data = array();
		$data['user'] = $this->User_model->getById($this->session->userdata('id'));
		$this->template->set('title', 'Profile User');
		$this->template->load('layout', 'contents' , 'admin/profile/profile', $data);
	}

	public function uploadPhoto_process() {
		$idUser = $this->session->userdata('id');

		$config['upload_path']		= './assets/upload/images/';
		$config['allowed_types']	= 'gif|jpg|jpeg|png';
		$config['file_name']			= $idUser.uniqid().time();
		$config['overwrite']			= TRUE;
		$config['max_size']				= 1024;

		$this->load->library('upload', $config);

		if($this->upload->do_upload('photo_profile')){
			$imgData =  $this->upload->data();
			//Compress Image
			$config2['image_library']='gd2';
			$config2['source_image']='./assets/upload/images/'.$imgData['file_name'];
			$config2['maintain_ratio']= FALSE;
			$config2['quality']= '80%';
			$config2['new_image']= './assets/upload/images/sq-'.$imgData['file_name'];
			if ($imgData['image_width'] > $imgData['image_height']) {
				$config2['width'] = $imgData['image_height'];
				$config2['height'] = $imgData['image_height'];
				$config2['x_axis'] = (($imgData['image_width'] / 2) - ($config2['width'] / 2));
			}
			else {
				$config2['height'] = $imgData['image_width'];
				$config2['width'] = $imgData['image_width'];
				$config2['y_axis'] = (($imgData['image_height'] / 2) - ($config2['height'] / 2));
			}
			$this->load->library('image_lib');
			$this->image_lib->clear();
			$this->image_lib->initialize($config2);
			$this->image_lib->crop();

			$this->User_model->updatePhotoProfile($idUser, $this->upload->data('file_name'));
			if($idUser == $this->session->userdata('id')) $this->session->set_userdata('photo', $this->upload->data('file_name'));
			$this->session->set_flashdata(array(
				'status' => 'success', 
				'message' => 'Congratulation. You have succesfully update a profile picture.'
			));
			redirect('dashboard/profile');
		} else{
			$this->session->set_flashdata(array(
				'status' => 'failed', 
				'message' => 'Sorry. Unfortunately the profile picture has not changed. Please check the file type and size and try to re:upload in a few moment. The maximum image size that accepted is 1MB and file extension are jpg/jpeg, png, gif.'
			));
			redirect('dashboard/profile');
		}
	}

}