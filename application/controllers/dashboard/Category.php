<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct(){
		parent::__construct();
		if(!$this->utils->isLogin()): redirect('auth/signin'); endif;
		$this->load->model('Category_model');
		$this->load->library('form_validation');
	}

	public function index() {
		$data = array();
		$data['categories'] = $this->Category_model->getAll();
		$this->template->set('title', 'Category');
		$this->template->load('layout', 'contents' , 'admin/category/overview', $data);		
	}

	public function add_process() {
		$category = $this->Category_model;
		$validation = $this->form_validation->set_rules($category->rules());
		if($validation->run()) {
			$category->create();
			$this->session->set_flashdata(array(
				'status'	=> 'success',
				'message'	=> 'Congratulation. You have succesfully create new category.'
			));
			redirect('dashboard/category');
		} else {
			$this->session->set_flashdata(array(
				'status'	=> 'failed',
				'message' => 'Sorry. Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'
			));
			$data = array();
			$data['categories'] = $this->Category_model->getAll();
			$this->template->set('title', 'Category');
			$this->template->load('layout', 'contents' , 'admin/category/overview', $data);	
		}
	}

	public function update_process($id = null) {
		if(!isset($id)) show_404();

		$catModel = $this->Category_model;
		$validation = $this->form_validation->set_rules($catModel->rules());

		if($validation->run()) {
			if($catModel->update($id)){
				$this->session->set_flashdata(array(
					'status'	=> 'success',
					'message'	=> 'Congratulation. You have succesfully updated a category.'
				));
			} else {
				$this->session->set_flashdata(array(
					'status'	=> 'failed',
					'message' => 'Sorry. Unfortunately the field has not been changed due to <b>Internal Error</b>. Please try again later.'
				));
			}
			redirect('dashboard/category');
		} else {
			$this->session->set_flashdata(array(
				'status'	=> 'failed',
				'message' => 'Sorry. Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'
			));
			$data = array();
			$data['categories'] = $this->Category_model->getAll();
			$this->template->set('title', 'Category');
			$this->template->load('layout', 'contents' , 'admin/category/overview', $data);
		}
	}

	public function delete($id = null){
		if(!isset($id)) show_404();

		if($this->Category_model->delete($id)){
			$this->session->set_flashdata(array(
				'status'	=> 'success',
				'message'	=> 'Congratulation. You have succesfully remove a category.'
			));
		} else {
			$this->session->set_flashdata(array(
				'status'	=> 'failed',
				'message'	=> 'Sorry. Unfortunately the user data still not removed.'
			));
		}
		redirect('dashboard/category');
	}

}