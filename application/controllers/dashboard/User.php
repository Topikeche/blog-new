<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->utils->isLogin()) redirect('auth/signin');
		if(!$this->utils->isAdmin()) show_error('','','');
		
		$this->load->model('User_model');
		$this->load->model('Role_model');
		$this->load->model('Sex_model');
		$this->load->library('form_validation');
	}

	public function index() {
		redirect('dashboard/user/list');
	}
	
	public function list() {
		$data = array();
		$data['user'] = $this->User_model->getAll();
		$this->template->set('title', 'User List');
		$this->template->load('layout', 'contents' , 'admin/user/user_list', $data);
	}

	public function detail($id = null){
		if(!isset($id)) show_404();

		$data = array();
		$data['user'] = $this->User_model->getById($id);
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit;
		$this->template->set('title', 'User Detail');
		$this->template->load('layout', 'contents' , 'admin/user/user_detail', $data);
	}

	public function add() {
		$data = array();
		$data['role'] = $this->Role_model->getAll();
		$data['sex'] = $this->Sex_model->getAll();
		$this->template->set('title', 'User Add');
		$this->template->load('layout', 'contents' , 'admin/user/user_add', $data);
	}
	public function add_process() {
		$user = $this->User_model;
		$validation = $this->form_validation->set_rules($user->rules());
		if($validation->run()) {
			$user->create();
			$this->session->set_flashdata(array('status' => 'success', 'message' => 'Congratulation. You have succesfully create new user.'));
			redirect('dashboard/user/list');
		} else{
			$this->session->set_flashdata(array('status' => 'failed', 'message' => 'Sorry. Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'));
			$data = array();
			$data['role'] = $this->Role_model->getAll();
			$data['sex'] = $this->Sex_model->getAll();
			$this->template->set('title', 'User Add');
			$this->template->load('layout', 'contents' , 'admin/user/user_add', $data);
		}
	}

	public function edit($id = null){
		if(!isset($id)) show_404();

		$data = array();
		$data['role'] = $this->Role_model->getAll();
		$data['sex'] = $this->Sex_model->getAll();
		$data['user'] = $this->User_model->getById($id);

		$this->template->set('title', 'User Edit');
		$this->template->load('layout', 'contents' , 'admin/user/user_edit', $data);
	}
	public function edit_process($id = null) {
		if(!isset($id)) show_404();

		$user = $this->User_model;
		$validation = $this->form_validation->set_rules($user->rulesForUpdate());
		if($validation->run()) {
			$user->update($id);
			$this->session->set_flashdata(array('status' => 'success', 'message' => 'Congratulation. You have succesfully update a user.'));
			redirect('dashboard/user/detail/'.$id);
		} else{
			$this->session->set_flashdata(array('status' => 'failed', 'message' => 'Sorry. Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'));
			$data = array();
			$data['user'] = $this->User_model->getById($id);
			$data['role'] = $this->Role_model->getAll();
			$data['sex'] = $this->Sex_model->getAll();
			$this->template->set('title', 'User Edit');
			$this->template->load('layout', 'contents' , 'admin/user/user_edit', $data);
		}
	}

	public function uploadPhoto_process($idUser = null) {
		if(!isset($idUser)) show_404();

		$config['upload_path']		= './assets/upload/images/';
		$config['allowed_types']	= 'gif|jpg|jpeg|png';
		$config['file_name']			= $idUser.uniqid().time();
		$config['overwrite']			= TRUE;
		$config['max_size']				= 1024;

		$this->load->library('upload', $config);

		if($this->upload->do_upload('photo_profile')){
			$this->User_model->updatePhotoProfile($idUser, $this->upload->data('file_name'));
			if($idUser == $this->session->userdata('id')) {
				$this->session->set_userdata('photo', $this->upload->data('file_name'));
			}
			$this->session->set_flashdata(array(
				'status' => 'success', 
				'message' => 'Congratulation. You have succesfully update a profile picture.'
			));
			redirect('dashboard/user/detail/'.$idUser);
		} else{
			$this->session->set_flashdata(array(
				'status' => 'failed', 
				'message' => 'Sorry. Unfortunately the profile picture has not changed. Please check the file type and size and try to re:upload in a few moment. The maximum image size that accepted is 1MB and file extension are jpg/jpeg, png, gif.'
			));
			redirect('dashboard/user/detail/'.$idUser);
		}
	}

	public function delete($id = null) {
		if(!isset($id)) show_404();
		
		if($this->User_model->delete($id)) {
			$this->session->set_flashdata(array(
				'status'	=> 'success', 
				'message' => 'Congratulation. You have succesfully remove a user.'
			));
		} else{
			$this->session->set_flashdata(array(
				'status'	=> 'failed', 
				'message' => 'Sorry. Unfortunately the user data still not removed.'
			));
		}
		redirect('dashboard/user/list');
	}
  
}
