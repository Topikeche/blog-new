<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->utils->isLogin()) redirect('auth/signin');

		$this->load->library('form_validation');
		$this->load->model('Category_model');
		$this->load->model('Post_model');
	}

	public function index() {
		redirect('dashboard/post/list');
	}

	public function detail($id = null) {
		if(!isset($id)) show_404();

		$data = array();
		$data['category'] = $this->Category_model->getAll();
		$data['post'] = $this->Post_model->getByIdFull($id);
		$this->template->set('title', 'Post Detail');
		$this->template->load('layout', 'contents' , 'admin/post/post_detail', $data);

	}

	public function list() {
		$data = array();
		$data['post'] = $this->Post_model->getAll();
		$this->template->set('title', 'List Post');
		$this->template->load('layout', 'contents' , 'admin/post/post_list', $data);
	}

	public function add() {
		$data = array();
		$data['category'] = $this->Category_model->getAll();
		$this->template->set('title', 'New Post');
		$this->template->load('layout', 'contents' , 'admin/post/post_add', $data);
	}

	public function add_process() {
		$post = $this->Post_model;
		$validation = $this->form_validation->set_rules($post->rules());
		if($validation->run()){
			$post->create();
			$this->session->set_flashdata([
				'status'	=> 'success',
				'message'	=> 'Congratulation. You have successfully add new post.'
			]);
			redirect('dashboard/post/list');
		} else{
			$this->session->set_flashdata([
				'status' => 'failed',
				'message' => 'Sorry! Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'
			]);
			$data = array();
			$data['category'] = $this->Category_model->getAll();
			$this->template->set('title', 'New Post');
			$this->template->load('layout', 'contents' , 'admin/post/post_add', $data);
		}
	}

	public function edit($id = null){
		if(!isset($id)) show_404();

		$data = array();
		$data['category'] = $this->Category_model->getAll();
		$data['post'] = $this->Post_model->getById($id);
		$this->template->set('title', 'Edit Post');
		$this->template->load('layout', 'contents' , 'admin/post/post_edit', $data);
	}

	public function edit_process($id = null){
		if(!isset($id)) show_404();

		$post = $this->Post_model;
		$validation = $this->form_validation->set_rules($post->rules());
		if($validation->run()){
			$post->update($id);
			$this->session->set_flashdata([
				'status'	=> 'success',
				'message'	=> 'Congratulation. You have successfully updated a post.'
			]);
			redirect('dashboard/post/detail/'.$id);
		// echo "<pre>";
		// var_dump("<b>ahai...</b>");
		// var_dump($data['post']);
		// echo "</pre>";
		// exit;
		} else {
			$this->session->set_flashdata(array(
				'status'	=> 'failed', 
				'message'	=> 'Sorry. Unfortunately there is still some invalid information. Please change the value of error fields and re:submit it.'
			));
			$data = array();
			$data['category'] = $this->Category_model->getAll();
			$data['post'] = $this->Post_model->getById($id);
			$this->template->set('title', 'Edit Post');
			$this->template->load('layout', 'contents' , 'admin/post/post_edit', $data);
		}
	}

	public function delete($id = null){
		if(!isset($id)) show_404();

		if($this->Post_model->delete($id)){
			$this->session->set_flashdata(array(
				'status'	=> 'success', 
				'message' => 'Congratulation. You have succesfully remove a post.'
			));
		} else{
			$this->session->set_flashdata(array(
				'status'	=> 'failed', 
				'message' => 'Sorry. Unfortunately the post data still not removed.'
			));
		}
		redirect('dashboard/post/list');
	}

}