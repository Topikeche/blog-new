<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= SITE_NAME.'-'.$title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/bootstrap/css/bootstrap.min.css') ?>">
  <!-- Date picker bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/font-awesome/css/font-awesome.min.css') ?>">
  <!-- select2 -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/select2/dist/css/select2.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/adminLTE/css/AdminLTE.min.css') ?>">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/custom.css') ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/adminLTE/css/skins/_all-skins.min.css') ?>">

  <!-- Additional CSS -->
  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- jQuery 3 -->
  <script src="<?= base_url('assets/dashboard/jquery/jquery.min.js') ?>" ></script>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

	<!-- This is header, like title. notif bar and user account -->
  <?php $this->load->view('admin/partial/header'); ?>
	<!-- =============================================== -->

	<!-- Left side column. contains the sidebar -->
  <?php $this->load->view('admin/partial/sidenav'); ?>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= $title ?>
        <!-- <small>Blank example to the fixed layout</small> -->
      </h1>
      <?php $this->load->view('admin/partial/breadcrumb') ?>
    </section>

    <!-- Main content -->
    <section class="content">

      <?php echo $contents; ?>
      
    </section>
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/dashboard/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- Date picker -->
<script src="<?= base_url('assets/dashboard/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Select2 -->
<script src="<?= base_url('assets/dashboard/select2/dist/js/select2.full.min.js') ?>"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/dashboard/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/dashboard/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/dashboard/adminLTE/js/adminlte.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/dashboard/adminLTE/js/demo.js') ?>"></script>
<!-- Text editor -->
<script src="<?= base_url('assets/dashboard/ckeditor4custom2/ckeditor.js') ?>"></script>
<script src="<?= base_url('assets/dashboard/ckfinder/ckfinder.js') ?>"></script>

<!-- Additional js -->


</body>
</html>