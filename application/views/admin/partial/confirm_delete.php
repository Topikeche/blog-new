<div class="modal fade" id="modal-confirm">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center">
				<i class="fa fa-trash-o big-icon pad-15"></i><br>
				Are you sure want to <b>DELETE</b> item <h3 id="nameItem" class="no-mar pad-top-5 pad-bot-5 text-red">?</h3>
				<small class="pad-top-10"><i>PS: You cannot undo this after click the button</i></small>
			</div>
			<div class="modal-footer">
				<a id="deleteItem" class="btn btn-danger">Yes, I'm sure</a>
			</div>
		</div>
	</div>
</div>