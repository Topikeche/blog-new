  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= ($this->session->userdata('photo') != NULL) ? base_url('assets/upload/images/sq-'.$this->session->userdata('photo')) : base_url('assets/dashboard/adminLTE/img/avatar4.png') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= word_limiter($this->session->userdata('short_name'), 2) ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">DASHBOARD</li>
        <li class="<?= $this->uri->segment(2) == 'overview' ? 'active': '' ?>">
          <a href="<?= site_url('dashboard/overview') ?>"><i class="fa fa-dashboard"></i> <span>Overview</span></a>
        </li>
				<li class="treeview <?= $this->uri->segment(2) == 'post' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>Post</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $this->uri->segment(3) == 'list' ? 'active': '' ?>"><a href="<?= base_url('dashboard/post/list') ?>"><i class="fa fa-circle-o"></i> List Post</a></li>
            <li class="<?= $this->uri->segment(3) == 'add' ? 'active': '' ?>"><a href="<?= base_url('dashboard/post/add') ?>"><i class="fa fa-circle-o"></i> Add Post</a></li>
          </ul>
        </li>
        <?php if($this->utils->isAdmin()): ?>
				<li class="treeview <?= $this->uri->segment(2) == 'user' ? 'active': '' ?>">
          <a href="#"><i class="fa fa-user"></i><span>User</span></a>
          <ul class="treeview-menu">
            <li class="<?= $this->uri->segment(3) == 'list' ? 'active': '' ?>">
              <a href="<?= site_url('dashboard/user/list') ?>"><i class="fa fa-circle-o"></i> List User</a>
            </li>
            <li class="<?= $this->uri->segment(3) == 'add' ? 'active': '' ?>">
              <a href="<?= site_url('dashboard/user/add') ?>"><i class="fa fa-circle-o"></i> Add User</a>
            </li>
          </ul>
        </li>
        <?php endif ?>
        <li class="<?= $this->uri->segment(2) == 'category' ? 'active': '' ?>">
          <a href="<?= base_url('dashboard/category') ?>"><i class="fa fa-tag"></i> <span>Category</span></a>
        </li>
        <li class="header">HOMEPAGE</li>
				<li class="treeview">
          <a href="#">
            <i class="fa fa-bookmark"></i>
            <span>Section</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> List User</a></li>
            <li><a href="collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Create</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->