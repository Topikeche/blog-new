<ol class="breadcrumb">
	<?php foreach($this->uri->segments as $segment): ?>
		<?= ($segment == reset($this->uri->segments) ? '<i class="fa fa-dashboard"></i>&nbsp;' : '') ?>
		<li class="<?= ($segment == end($this->uri->segments) ? 'active' : '') ?>">
			<?= (strpos($segment, '_') !== FALSE)? ucfirst(strstr($segment, '_', true)) : ucfirst($segment) ?>
		</li>
	<?php endforeach ?>
</ol>