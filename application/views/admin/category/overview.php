<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<div class="row">
	<div class="col-sm-5 col-md-6">
	
		<?php $this->load->view('admin/category/category_add') ?>

	</div>
	<div class="col-sm-7 col-md-6">
		
		<?php $this->load->view('admin/category/category_list', $categories) ?>

	</div>
</div>