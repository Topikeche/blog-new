<div class="box">
	<div class="box-header">
		<h3 class="box-title">Add New Category</h3>
	</div>
	<div class="box-body">
		<form action="<?= base_url('dashboard/category/add_process') ?>" method="post">
			<div class="form-horizontal">
				<div class="form-group <?= form_error('category')? 'has-error' : '' ?>">
					<div class="col-sm-12">
						<input type="text" name="category" class="form-control" value="<?= set_value('category') ?>" placeholder="New category" />
						<?= form_error('category', '<small class="text-red">', '</small>'); ?>
					</div>
				</div>
			</div>
			<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Create</button>
		</form>
	</div>
</div>