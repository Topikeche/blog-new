<style>
.d-table{width:100%;margin-bottom:20px;}
.d-head{font-weight:700;background-color: #f1f1f1;}
.d-row{display: grid;width:100%;}
.d-row-3{grid-template-columns: 40px auto 83px;}
.d-col{padding:8px;}
.d-table .d-col .btn{margin:0 1px;}
.d-table .panel{border-radius:0px;margin-bottom:0px;}
.d-table .panel:nth-child(even) {background-color: #f1f1f1};
</style>
<div class="box">
	<div class="box-header">
		<h3 class="box-title">List Categories</h3>
	</div>
	<div class="box-body">

<div class="d-table">
	<div class="d-head">
		<div class="d-row d-row-3">
			<div class="d-col"><input type="checkbox"></div>
			<div class="d-col">Category</div>
			<div class="d-col"></div>
		</div>
	</div>
	<div id="accordions" class="d-body">
	<?php foreach ($categories as $single) : ?>
		<div class="panel">
			<div class="d-row d-row-3">
				<div class="d-col" style="padding-top:12px;"><input type="checkbox"></div>
				<div class="d-col" style="padding-top:12px;"><?= $single->category ?></div>
				<div class="d-col">
					<div class="row no-mar">
						<a data-toggle="collapse" data-parent="#accordions" href="#collapse-<?= $single->id ?>">
							<span class="btn btn-sm btn-social-icon btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></span>
						</a>
						<span data-target="#modal-confirm" data-toggle="modal">
							<a class="btn btn-sm btn-social-icon btn-danger" onclick="deleteLink('<?= base_url('dashboard/category/delete/'.$single->id) ?>', '<?= $single->category ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></a>
						</span>
					</div>
				</div>
			</div>
			<div id="collapse-<?= $single->id ?>" class="panel-collapse collapse">
				<form action="<?= base_url('dashboard/category/update_process/'.$single->id) ?>" method="post">
				<div  class="d-row d-row-3">
					<div class="d-col"></div>
					<div class="d-col"><input type="text" name="category" class="form-control" value="<?= $single->category ?>" /></div>
					<div class="d-col"><button class="btn btn-primary full" type="submit">Save</button></div>
				</div>
				</form>
			</div>
		</div>
	<?php endforeach ?>
	</div>
</div>
<div class="box-footer clearfix">
	<ul class="pagination pagination-sm no-margin pull-right">
		<li><a href="#">&laquo;</a></li>
		<li><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">&raquo;</a></li>
	</ul>
</div>

</div>


<?php $this->load->view('admin/partial/confirm_delete') ?>

<script>
function deleteLink(link, name){
	document.getElementById('deleteItem').setAttribute("href", link)
	document.getElementById('nameItem').innerHTML = name.toUpperCase()
}
function collapseOther(){
	let lastActive = document.getElementsByClassName('collapse in')
	lastActive[1].classList.remove('in')
	console.log(lastActive)
}
</script>