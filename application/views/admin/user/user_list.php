<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<div class="box-body">
		<div class="table-responsive">
		<table class="table table-striped table-hover">
			<tr>
				<th>ID</th>
				<th>Full Name</th>
				<th>Sex</th>
				<th>Contact</th>
				<th>Address</th>
				<th style="min-width:130px;"></th>
			</tr>
			<?php foreach ($user as $single) : ?>
			<tr>
				<td><?= $single->id ?></td>
				<td>
					<a href="<?= base_url('dashboard/user/detail/'.$single->id) ?>"><?= $single->full_name; ?></a>
				</td>
				<td><?= ($single->sex == 1) ? '<i class="fa fa-mars text-aqua bold"></i>' : '<i class="fa fa-venus text-pink bold"></i>' ?></td>
				<td><?= $single->email ?><br><i><?= $single->phone ?></i></td>
				<td><?= $single->address ?></td>
				<td class="button-list">
					<div class="row no-mar">
					<a href="<?= base_url('dashboard/user/detail/'.$single->id) ?>" class="btn btn-sm btn-social-icon btn-primary" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
					<a href="<?= base_url('dashboard/user/edit/'.$single->id) ?>" class="btn btn-sm btn-social-icon btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
					<span data-target="#modal-confirm" data-toggle="modal">
						<a class="btn btn-sm btn-social-icon btn-danger" onclick="deleteLink('<?= base_url('dashboard/user/delete/'.$single->id) ?>', '<?= $single->full_name ?>')" data-toggle="tooltip" title="Delete">
							<i class="fa fa-trash-o"></i>
						</a>
					</span>
					</div>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
		</div>
	</div>
	<!-- /.box-body -->
	<!-- <div class="box-footer">Footer</div> -->
</div>

<!-- delete confirm modal -->
<?php $this->load->view('admin/partial/confirm_delete'); ?>

<script>
function deleteLink(link, name){
	document.getElementById('deleteItem').setAttribute("href", link)
	document.getElementById('nameItem').innerHTML = name.toUpperCase()
}
</script>