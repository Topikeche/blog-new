<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<form action="<?= base_url('dashboard/post/edit_process/'.$post->id) ?>" method="post">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<h4>Content</h4>
				<div class="mar-bot-10 ">
					<input type="text" name="title" class="form-control <?= form_error('title')? 'has-error' : '' ?>" value="<?= (set_value('title'))? set_value('title') : $post->title ?>" placeholder="Title goes here" />
					<?= form_error('title', '<small class="text-red">', '</small>'); ?>
				</div>
				<div class="pad-top-10 pad-bot-10 <?= form_error('content')? 'has-error' : '' ?>">
					<textarea id="editor1" name="content" rows="15" cols="80"><?= (set_value('content')? set_value('content') : $post->content) ?></textarea>
					<?= form_error('content', '<small class="text-red">', '</small>'); ?>
				</div>
			</div>
			<div class="col-sm-5 col-md-4">
				<h4>Thumbnail Image</h4>
				<div class="post-thumb">
					<div class="img-wrap">
						<?php
						$imgPath;
						if(set_value('feature-image')) {
							$imgPath = set_value('feature-image');
						} else {
							if($post->feature_image != NULL) $imgPath = $post->feature_image;
							else $imgPath = base_url('assets/upload/images/no-images.png');
						}
						?>
						<img id="preview" src="<?= $imgPath ?>" alt="">
						<div class="img-overlay"></div>
						<div class="img-upload">
							<label for="">
							<span onclick="selectImage()" class="btn btn-lg btn-primary" data-toggle="tooltip" title="Add Image"><i class="fa fa-image"></i></span>
							</label>			
						</div>
					</div>
				</div>
				<input type="text" id="feature-image" name="feature-image" value="<?= (set_value('feature_image'))? set_value('feature-image') : $post->feature_image ?>" class="hidden" />
				<h4 class="pad-top-10">Category</h4>
				<div class="<?= form_error('category[]')? 'has-error' : '' ?>">
					<?php $postCats = explode(',', $post->category) ?>
					<select class="form-control select2" name="category[]" multiple="multiple" data-placeholder="Select some category"style="width: 100%;">
						<?php 
						if(set_value('category')) {
							foreach($category as $single) : 
								echo "<option value=".$single->id." ".set_select('category[]', $single->id)." >".$single->category."</option>";
							endforeach; 
						} else{
							foreach($category as $single) : 
								$selected = (in_array($single->id, $postCats)) ? 'selected' : '';
								echo "<option value=".$single->id." ".$selected.">".$single->category."</option>";
							endforeach;
						}
						?>
					</select>
					<?= form_error('category[]', '<small class="text-red">', '</small>'); ?>
				</div>
				<h4 class="pad-top-10 ">Tags</h4>
				<input type="text" name="tags" class="form-control mar-bot-10" placeholder="Separate with coma's whitout spaces" value="<?= (set_value('tags'))? set_value('tags') : $post->tags ?>" />
				<div class="pad-top-10">
					<div class="material-switch">
						<?php
						$checked;
						if(set_value('status') != ''){
							if(set_value('status') == '1') $checked = 'checked';
							else $checked = '';
						} else {
							if($post->status == '1') $checked = 'checked';
							else $checked = '';
						}
						?>
						<input type="hidden" name="status" value="<?= (set_value('status') != '')? set_value('status') : $post->status ?>"><input id="switch-publish" name="shadow" onclick="this.previousSibling.value=1-this.previousSibling.value" type="checkbox" <?= $checked ?>/>
						<label for="switch-publish" class="label-primary"></label>
						<span id="switch-label" style="width:160px;margin-top:1px;" class="badge pull-right"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save</button>
	</div>
	</form>
</div>

<script>
	function selectImage() {
		CKFinder.popup({
			chooseFiles: true,
			width: 800,
			height: 500,
			onInit: function( finder ) {
				console.log('masok');
				finder.on( 'files:choose', function( evt ) {
					var file = evt.data.files.first();
					console.log('lalalalal: ', file);
					document.getElementById('feature-image').value = file.getUrl();
					document.getElementById('preview').src = file.getUrl();
					console.log(file.getUrl())
					// output
				});
				finder.on( 'file:choose:resizedImage', function( evt ) {
					document.getElementById('feature-image').value = evt.data.resizedUrl;
				});
			}
		});
	}

	//switch label
	if($('#switch-publish').is(":checked")) $('#switch-label').addClass('bg-light-blue').text('Will be Published')
	else $('#switch-label').removeClass('bg-light-blue').text('Will be Saved to Draft')
	$('#switch-publish').change(function(){
		if($(this).is(":checked")) $('#switch-label').addClass('bg-light-blue').text('Will be Published')
		else $('#switch-label').removeClass('bg-light-blue').text('Will be Saved to Draft')
	})

	$(function () {
		//initialize select2
		$('.select2').select2();
		//call ckeditor
		CKEDITOR.replace('editor1', {
			filebrowserBrowseUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/ckfinder.html',
			filebrowserUploadUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
		})		
  })
</script>