<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th style="width:30px;"><input type="checkbox"></th>
					<th>Title</th>
					<th style="width:30%;">Content</th>
					<th style="width:15%;">Category</th>
					<th></th>
					<th>Author</th>
					<th style="width:120px;"></th>
				</tr>
				<?php foreach($post as $single): ?>
				<tr>
					<td><input type="checkbox"></td>
					<td>
						<a href="<?= base_url('dashboard/post/detail/'.$single->id) ?>"><?= character_limiter($single->title, 15) ?></a>
					</td>
					<td><?= character_limiter(strip_tags($single->content), 80) ?></td>
					<td>
						<?php
						$arrayCategory = explode(",", $single->category);
						foreach ($arrayCategory as $value) {
							echo '<span class="label label-primary">'.$value.'</span> ';
						}
						?>
					</td>
					<td>
						<?php
						if($single->status == 1) echo '<i class="fa fa-circle text-green" data-toggle="tooltip" title="Published"></i>';
						else echo '<i class="fa fa-circle text-grey" data-toggle="tooltip" title="Drafted"></i>';
						?>
					</td>
					<td>
						<a href="<?= base_url('dashboard/user/detail/'.$single->author) ?>"><?= character_limiter($single->full_name, 15) ?></a><br>
						<?php $create = new DateTime($single->created_at) ?>
						<small><i>At <?= $create->format('j M Y, h:i A') ?></i></small>
					</td>
					<td class="button-list">
						<div class="row no-mar">
						<a href="<?= base_url('dashboard/post/detail/'.$single->id) ?>" class="btn btn-sm btn-social-icon btn-primary" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
						<a href="<?= base_url('dashboard/post/edit/'.$single->id) ?>" class="btn btn-sm btn-social-icon btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
						<span data-target="#modal-confirm" data-toggle="modal">
							<a class="btn btn-sm btn-social-icon btn-danger" onclick="deleteLink('<?= base_url('dashboard/post/delete/'.$single->id) ?>', '<?= $single->title ?>')" data-toggle="tooltip" title="Delete">
								<i class="fa fa-trash-o"></i>
							</a>
						</span>
						</div>
					</td>
				</tr>
				<?php endforeach ?>
			</table>
		</div>
	</div>
</div>

<!-- delete confirm modal -->
<?php $this->load->view('admin/partial/confirm_delete'); ?>

<script>
function deleteLink(link, name){
	document.getElementById('deleteItem').setAttribute("href", link)
	document.getElementById('nameItem').innerHTML = name.toUpperCase()
}
</script>