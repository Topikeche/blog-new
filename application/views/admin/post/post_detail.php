<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<div class="box-header no-padding">
		<div class="feature-image">
			<img src="<?= ($post->feature_image != NULL)? $post->feature_image : base_url('assets/upload/images/default.jpg') ?>" alt="">
			<div class="publish-status">
				<i class="fa fa-circle text-<?= ($post->status == '1')? 'green' : 'grey' ?>"></i>&nbsp;&nbsp;
				<b><?= ($post->status == '1')? 'PUBLISHED' : 'NOT PUBLISHED' ?></b>
			</div>
			<div class="title">
			</div>
		</div>
		<div class="meta">
			<div class="row no-margin">
				<div class=col-xs-9>
					<div class="author">
						<div class="pull-left image">
							<img src="<?= ($post->photo != NULL)? base_url('assets/upload/images/sq-'.$post->photo) : base_url('assets/dashboard/adminLTE/img/avatar4.png') ?>" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<b><a href="<?= base_url('dashboard/user/detail/'.$post->author) ?>"><?= $post->full_name ?></a></b><br>
							<small><?= $post->role ?></small>
						</div>
					</div>
				</div>
				<div class=col-xs-3>
					<div class="input-group-btn custom">
						<button type="button" class="btn btn-default btn-lg btn-dot3 dropdown-toggle pull-right" data-toggle="dropdown">
							<span class="fa fa-ellipsis-v"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li><a href="<?= base_url('dashboard/post/edit/'.$post->id) ?>"><i class="fa fa-edit"></i>&nbsp;Edit</a></li>
							<li data-target="#modal-confirm" data-toggle="modal"><a onclick="deleteLink('<?= base_url('dashboard/post/delete/'.$post->id) ?>', '<?= $post->title ?>')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete</a></li>
							<li class="divider"></li>
							<li class="disabled"><a href="#"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-body">
		<h3 class="mar-top-5 mar-bot-20"><b><?= ucwords($post->title) ?></b></h3>
		<?= $post->content ?>
	</div>
	<div class="box-footer">
		<i class="fa fa-tags"></i>&nbsp;&nbsp;<b>Tags:</b>&nbsp;&nbsp;
		<?php
		if($post->tags !== ""){
			$tags = explode(',', $post->tags);
			foreach($tags as $tag):
				echo '<span class="btn btn-xs btn-primary">'.$tag.'</span>&nbsp;&nbsp;';
			endforeach;
		}
		?>
	</div>
</div>

<!-- delete confirm modal -->
<?php $this->load->view('admin/partial/confirm_delete'); ?>

<script>
function deleteLink(link, name){
	document.getElementById('deleteItem').setAttribute("href", link)
	document.getElementById('nameItem').innerHTML = name.toUpperCase()
}
</script>