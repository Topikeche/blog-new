<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<form action="<?= base_url('dashboard/post/add_process') ?>" method="post">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<h4>Content</h4>
				<div class="mar-bot-10 ">
					<input type="text" name="title" class="form-control <?= form_error('title')? 'has-error' : '' ?>" value="<?= set_value('title') ?>" placeholder="Title goes here" />
					<?= form_error('title', '<small class="text-red">', '</small>'); ?>
				</div>
				<div class="pad-top-10 pad-bot-10 <?= form_error('content')? 'has-error' : '' ?>">
					<textarea id="editor1" name="content" rows="15" cols="80"><?= set_value('content') ?></textarea>
					<?= form_error('content', '<small class="text-red">', '</small>'); ?>
				</div>
			</div>
			<div class="col-sm-5 col-md-4">
				<h4>Thumbnail Image</h4>
				<div class="post-thumb">
					<div class="img-wrap">
						<img id="preview" src="<?= (set_value('feature-image')) ? set_value('feature-image') : base_url('assets/upload/images/no-images.png') ?>" alt="">
						<div class="img-overlay"></div>
						<div class="img-upload">
							<label for="">
							<span onclick="selectImage()" class="btn btn-lg btn-primary" data-toggle="tooltip" title="Add Image"><i class="fa fa-image"></i></span>
							</label>			
						</div>
					</div>
				</div>
				<input type="text" id="feature-image" name="feature-image" class="hidden" />
				<h4 class="pad-top-10">Category</h4>
				<div class="<?= form_error('category[]')? 'has-error' : '' ?>">
					<select class="form-control select2" name="category[]" multiple="multiple" data-placeholder="Select some category"style="width: 100%;">
						<?php foreach($category as $single) : ?>
						<option value="<?= $single->id ?>" <?= set_select('category[]', $single->id) ?>><?= $single->category ?></option>
						<?php endforeach ?>
					</select>
					<?= form_error('category[]', '<small class="text-red">', '</small>'); ?>
				</div>
				<h4 class="pad-top-10 ">Tags</h4>
				<input type="text" name="tags" value="<?= set_value('tags') ?>" class="form-control mar-bot-10" placeholder="Separate with coma's whitout spaces" />
				<div class="pad-top-10">
					<div class="material-switch">
						<?php
						if(set_value('status') != ''){
							if(set_value('status') == '1') $isChecked = 'checked';
							else $isChecked = '';
						} else {
							$isChecked = 'checked';
						}
						?>
						<input type="hidden" name="status" value="<?= (set_value('status') != '') ? set_value('status') : '1' ?>"><input id="switch-publish" onclick="this.previousSibling.value=1-this.previousSibling.value" type="checkbox" <?= $isChecked ?>/>
						<label for="switch-publish" class="label-primary"></label>
						<span id="switch-label" style="width:160px;margin-top:1px;" class="badge pull-right"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Create</button>
	</div>
	</form>
</div>

<!-- <div class="post-thumb">
	<div class="img-wrap">
		<img id="preview" src="assets/upload/images/no-images.png" alt="">
		<div class="img-overlay"></div>
		<div class="img-upload">
			<label for="upload-image">
				<span class="btn btn-lg btn-primary" data-toggle="tooltip" title="Add Image"><i class="fa fa-image"></i></span>
				<input type="file" onchange="loadImage(event)" id="upload-image" name="feature-image" class="hidden">
			</label>
		</div>
	</div>
</div> -->

<script>
	// ============= PENTING ============= !!!
	// function loadImage(e){
	// 	console.log(e)
	// 	let previewImage = document.getElementById('preview')
	// 	previewImage.src = URL.createObjectURL(event.target.files[0])
	// }

	function selectImage() {
		CKFinder.popup({
			chooseFiles: true,
			width: 800,
			height: 500,
			onInit: function( finder ) {
				console.log('masok');
				finder.on( 'files:choose', function( evt ) {
					var file = evt.data.files.first();
					console.log('lalalalal: ', file);
					document.getElementById('feature-image').value = file.getUrl();
					document.getElementById('preview').src = file.getUrl();
					console.log(file.getUrl())
					// output
				});
				finder.on( 'file:choose:resizedImage', function( evt ) {
					document.getElementById('feature-image').value = evt.data.resizedUrl;
				});
			}
		});
	}

	$(function () {
		//switch label
		if($('#switch-publish').is(":checked")) $('#switch-label').addClass('bg-light-blue').text('Will be Published')
		else $('#switch-label').removeClass('bg-light-blue').text('Will be Saved to Draft')
		$('#switch-publish').change(function(){
			if($(this).is(":checked")) $('#switch-label').addClass('bg-light-blue').text('Will be Published')
			else $('#switch-label').removeClass('bg-light-blue').text('Will be Saved to Draft')
		})
		//initialize select2
		$('.select2').select2();
		//call ckeditor
		CKEDITOR.replace('editor1', {
			filebrowserBrowseUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/ckfinder.html',
			filebrowserUploadUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl: '<?php echo base_url() ?>assets/dashboard/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
		})		
  })
</script>