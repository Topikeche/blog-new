<?php 
$CI =& get_instance();
if(!isset($CI)) : $CI = new CI_Controller(); endif;
$CI->load->helper('url');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Access Denied (403)</title>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/bootstrap/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dashboard/adminLTE/css/AdminLTE.min.css') ?>">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= base_url('assets/error.min.css') ?>">
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>403</h1>
				</div>
				<h2>Access denied!</h2>
				<p>You do not have the permission to access this page on this server. Make sure you have tha access by logging in or contact the administrator. <a href="<?= base_url('auth/signin') ?>">Return to homepage</a></p>
				<div class="notfound-social">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
			</div>
		</div>
	</div>
</body>
</html>
