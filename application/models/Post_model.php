<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends CI_Model {

	public function rules() {
		return array(
			['field' => 'title', 'label' => 'Title', 'rules' => 'trim|required|max_length[100]'],
			['field' => 'content', 'label' => 'Content', 'rules' => 'trim|required'],
			['field' => 'category[]', 'label' => 'Category', 'rules' => 'trim|required']
		);
	}

	public function getAll() {
		$this->db->select('p.id, p.title, p.content, p.created_at, p.feature_image, p.status, p.author, GROUP_CONCAT(c.category) as category, u.full_name');
		$this->db->from('post p');
		if(!$this->utils->isAdmin()){
			$this->db->where('p.author', $this->session->userdata('id'));
		}
		$this->db->join('post_category pc', 'pc.id_post=p.id', 'left');
		$this->db->join('category c', 'pc.id_category=c.id', 'left');
		$this->db->join('user u', 'p.author=u.id', 'left');
		$this->db->order_by('created_at', 'DESC');
		$this->db->group_by('p.id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getById($idPost) {
		$this->db->select('p.*, GROUP_CONCAT(pc.id_category) AS category');
		$this->db->from('post p');
		$this->db->where('p.id', $idPost);
		$this->db->join('post_category pc', 'pc.id_post=p.id', 'left');
		$this->db->group_by('p.id');
		$query = $this->db->get();
		return $query->row();
	}

	public function getByIdFull($idPost) {
		$this->db->select('p.*, GROUP_CONCAT(c.category) AS category, u.full_name, u.photo, r.role');
		$this->db->from('post p');
		$this->db->where('p.id', $idPost);
		$this->db->join('post_category pc', 'pc.id_post=p.id', 'left');
		$this->db->join('category c', 'pc.id_category=c.id', 'left');
		$this->db->join('user u', 'p.author=u.id', 'left');
		$this->db->join('role r', 'u.role=r.id', 'left');
		$this->db->group_by('p.id');
		$query = $this->db->get();
		return $query->row();
	}

	public function create() {
		$post = $this->input->post();
		$data['title']					= ucwords($post['title']);
		$data['content']				= $post['content'];
		$data['feature_image']	= ($post['feature-image'] == "") ? NULL : $post['feature-image'];
		$data['tags']						= ($post['tags'] == "") ? "" : str_replace(", ", ",", $post['tags']);
		$data['status']					= $post['status'];
		$data['author']					= $this->session->userdata('id');
		$data['url_path']				= preg_replace('~\s+~', '-', strtolower($post['title']));

		$this->db->trans_start();

		$this->db->insert('post', $data);
		$idPost = $this->db->insert_id();
		$data2 = array();
		foreach($post['category'] as $single):
			array_push($data2, array(
				'id_post' 		=> $idPost,
				'id_category'	=> $single
			));
		endforeach;
		$this->db->insert_batch('post_category', $data2);

		$this->db->trans_complete();
	}

	public function update($idPost) {
		// $now = date('Y-m-d H:i:s');
		$post = $this->input->post();

		$data['title']					= ucwords($post['title']);
		$data['content']				= $post['content'];
		$data['feature_image']	= ($post['feature-image'] == "") ? NULL : $post['feature-image'];
		$data['tags']						= ($post['tags'] == "") ? "" : str_replace(", ", ",", $post['tags']);
		$data['status']					= $post['status'];
		$data['url_path']				= preg_replace('~\s+~', '-', strtolower($post['title']));

		$this->db->trans_start();

		$this->db->update('post', $data, ['id' => $idPost]);
		$this->db->delete('post_category', ['id_post' => $idPost]);
		$data2 = array();
		foreach($post['category'] as $single):
			array_push($data2, array(
				'id_post' 		=> $idPost,
				'id_category'	=> $single
			));
		endforeach;
		$this->db->insert_batch('post_category', $data2);

		$this->db->trans_complete();
	}

	public function delete($idPost) {
		$this->db->trans_start();
		$this->db->delete('post_category', ['id_post' => $idPost]);
		$this->db->delete('post', ['id' => $idPost]);
		$this->db->trans_complete();
		return $this->db->trans_status();
	}

}