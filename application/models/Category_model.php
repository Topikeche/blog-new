<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	public function rules() {
		return [
			[
				'field' => 'category', 
				'label' => 'Category', 
				'rules' => 'trim|required|is_unique[category.category]',
				'errors'	=> array('is_unique' => 'This %s is already been added.')
			]
		];
	}

	public function getAll() {
		$this->db->select('*');
		$this->db->from('category');
		$this->db->order_by('category', 'ASC');
		$query = $this->db->get();
		return $query->result();
		// return $this->db->get('category')->result();
	}

	public function create() {
		$post = $this->input->post();
		$data['category'] = ucfirst($post['category']);
		$this->db->insert('category', $data);
	}

	public function update($idCategory) {
		$post = $this->input->post();
		$data['category'] = ucfirst($post['category']);
		return $this->db->update('category', $data, ['id' => $idCategory]);
	}

	public function delete($idCategory) {
		return $this->db->delete('category', ['id' => $idCategory]);
	}
	
}
