<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function rules() {
		return [
			['field' => 'full_name', 'label' => 'Full Name', 'rules' => 'trim|required'],
			[
				'field'		=> 'email', 
				'label'		=> 'Email', 
				'rules'		=> 'trim|required|valid_email|is_unique[user.email]',
				'errors'	=> array('is_unique' => 'The %s is already been registered.')
			],
			['field' => 'phone', 'label' => 'Phone', 'rules' => 'trim|required|min_length[10]|max_length[13]|numeric'],
			['field' => 'sex', 'label' => 'Sex', 'rules' => 'trim|required'],
			['field' => 'birth_date', 'label' => 'Birth Date', 'rules' => 'trim|required'],
			['field' => 'address', 'label' => 'Address', 'rules' => 'trim|required'],
			['field' => 'role', 'label' => 'Role', 'rules' => 'trim|required'],
			['field' => 'password', 'label' => 'Password', 'rules' => 'trim|required|min_length[6]'],
			[
				'field'		=> 'password_confirm', 
				'label'		=> 'Confirm Password', 
				'rules'		=> 'trim|required|matches[password]',
				'errors'	=> array('matches' => 'The Confirm Password doesn\'t match the Password.')
			]
		];
	}

	public function rulesForUpdate() {
		return [
			['field' => 'full_name', 'label' => 'Full Name', 'rules' => 'trim|required'],
			['field' => 'phone', 'label' => 'Phone', 'rules' => 'trim|required|min_length[10]|max_length[13]|numeric'],
			['field' => 'sex', 'label' => 'Sex', 'rules' => 'trim|required'],
			['field' => 'birth_date', 'label' => 'Birth Date', 'rules' => 'trim|required'],
			['field' => 'address', 'label' => 'Address', 'rules' => 'trim|required'],
			['field' => 'role', 'label' => 'Role', 'rules' => 'trim|required']
		];
	}

	public function rulesForSignin() {
		return [
			['field' => 'email', 'label' => 'Password', 'rules' => 'trim|required|valid_email'],
			['field' => 'password', 'label' => 'Password', 'rules' => 'trim|required|min_length[6]'],
		];
	}

	public function getAll() {
		return $this->db->query("SELECT id, full_name, sex, email, phone, address FROM user")->result();
	}

	public function getById($idUser) {
		// return $this->db->get_where('user', ['id' => $idUser])->row();
		// return $this->db->query("SELECT * FROM `user` WHERE `id`=$idUser")->row();
		$this->db->select('user.*,sex.sex AS the_sex, role.role AS the_role');
		$this->db->from('user');
		$this->db->where('user.id', $idUser);
		$this->db->join('sex', 'user.sex=sex.id', 'left');
		$this->db->join('role', 'user.role=role.id', 'left');
		$query = $this->db->get();
		return $query->row();
	}

	public function create() {
		$post = $this->input->post();
		$data['full_name']	= $post['full_name'];
		$data['email']			= $post['email'];
		$data['phone']			= $post['phone'];
		$data['sex']				= $post['sex'];
		$data['birth_date'] = $post['birth_date'];
		$data['address']		= $post['address'];
		$data['role']				= $post['role'];
		$data['password']		= md5($post['password']);
		$this->db->insert('user', $data);
	}

	public function update($idUser) {
		$post = $this->input->post();
		$data['full_name']	= $post['full_name'];
		$data['phone']			= $post['phone'];
		$data['sex']				= $post['sex'];
		$data['birth_date'] = $post['birth_date'];
		$data['address'] 		= $post['address'];
		$data['role'] 			= $post['role'];
		$this->db->update('user', $data, array('id' => $idUser));
	}

	public function updatePhotoProfile($idUser, $fileName) {
		$data['photo'] = $fileName;
		$this->db->update('user', $data, array('id' => $idUser));
	}

	public function delete($idUser) {
		return $this->db->delete('user', ['id' => $idUser]);
	}

	public function authData() {
		$email		= $this->input->post('email');
		$password = $this->input->post('password');
		return $this->db->get_where('user', ['email' => $email, 'password' => md5($password)])->row();
	}

}